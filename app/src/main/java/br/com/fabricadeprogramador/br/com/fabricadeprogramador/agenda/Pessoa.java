package br.com.fabricadeprogramador.br.com.fabricadeprogramador.agenda;

import java.io.Serializable;

/**
 * Created by Everton on 28/10/2016.
 */

public class Pessoa implements Serializable {
    private Integer id;
    private String nome;
    private Integer imagem;
    private String email;
    private String telefone;

    public Pessoa() {

    }

    public Pessoa(String nome, int imagem) {
        this.nome = nome;
        this.imagem = imagem;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getImagem() {
        return imagem;
    }

    public void setImagem(Integer imagem) {
        this.imagem = imagem;
    }
}
