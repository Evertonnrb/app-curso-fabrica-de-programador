package br.com.fabricadeprogramador.br.com.fabricadeprogramador.agenda;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Everton on 28/10/2016.
 */

public class BancoDeDados extends SQLiteOpenHelper{
    /**
     * SQLiteOpenHelper implementa 2 construtores
     *
     * @param db
     */
    private static final String NOME_BANCO = "pessoa_db";
    //Versao do banco
    private static final int VERSAO_BANCO = 1;
    //private final String TABELA = "tbpessoa";

    //Sobrecarregando um construtor passando apenas o contexto que a view instanciara
    public BancoDeDados(Context context) {
        super(context, NOME_BANCO,null,VERSAO_BANCO);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    String sql = "create table tbpessoa(id integer primary key,nome text,email text,telefone text)";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {String sql = "drop table if exists tbpessoa";
        db.execSQL(sql);
        onCreate(db);
    }
    public  void salvar(Pessoa pessoa){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("nome",pessoa.getNome());
        values.put("email",pessoa.getEmail());
        values.put("telefone",pessoa.getTelefone());

        db.insert("tbpessoa",null,values);
        db.close();

    }
    public List<Pessoa> buscarTodos(){
        List<Pessoa> lista = new ArrayList<>();
        String sql = "select*from tbpessoa";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql,null);
        if (cursor.moveToFirst()) {
            do {
                //Pega do cursor e coloca no obj pessoa
                Pessoa pessoa = new Pessoa();
                pessoa.setId(cursor.getInt(0));
                pessoa.setNome(cursor.getString(1));
                pessoa.setEmail(cursor.getString(2));
                pessoa.setTelefone(cursor.getString(3));
                //coloca na lista
                lista.add(pessoa);

            }while (cursor.moveToNext());
        }


        return lista;
    }
}
