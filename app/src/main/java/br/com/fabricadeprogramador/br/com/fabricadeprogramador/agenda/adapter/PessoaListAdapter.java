package br.com.fabricadeprogramador.br.com.fabricadeprogramador.agenda.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import br.com.fabricadeprogramador.appfdp_online.R;
import br.com.fabricadeprogramador.br.com.fabricadeprogramador.agenda.Pessoa;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Everton on 28/10/2016.
 */

public class PessoaListAdapter extends ArrayAdapter<Pessoa> {
    public PessoaListAdapter(Context context, List<Pessoa> pessoaList) {
        super(context, R.layout.activity_item_pessoa);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView==null){
            convertView = View.inflate(getContext(),R.layout.activity_item_pessoa,null);
            holder = new ViewHolder();
            ButterKnife.bind(holder,convertView);
            //vinculando holder ao contenti view
            convertView.setTag(holder);
        }else{
            //reaproveitando o holder
            holder = (ViewHolder)convertView.getTag();

        }
        Pessoa pessoa = getItem(position);
        if (pessoa!=null){
            holder.nome_pessoa.setText(pessoa.getNome());
            holder.imagem.setImageResource(pessoa.getImagem());
        }
        return convertView;
    }
    class ViewHolder{
        @Bind(R.id.imagem_pessoa )
        ImageView imagem;
        @Bind(R.id.pessoa_nome)
        TextView nome_pessoa;
    }
}
