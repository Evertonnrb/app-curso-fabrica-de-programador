package br.com.fabricadeprogramador.br.com.fabricadeprogramador.agenda;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringDef;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.com.fabricadeprogramador.appfdp_online.R;
import br.com.fabricadeprogramador.br.com.fabricadeprogramador.agenda.adapter.PessoaListAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Everton on 16/10/2016.
 */

public class AgendaListaActivity extends AppCompatActivity {
    @Bind(R.id.id_agenda)
    ListView lista;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_agenda);

        ButterKnife.bind(this);

        Intent i  = getIntent();
        Bundle bundle = i.getExtras();
        String nome = bundle.getString("nome");
        GerenciaDeContatos.adicionar(new Pessoa(nome,R.drawable.logofdpl));


        //ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,GerenciaDeContatos.getNomes());
        PessoaListAdapter adapter = new PessoaListAdapter(this,GerenciaDeContatos.getPessoaList());
        lista.setAdapter(adapter);
    }
    @OnClick(R.id.bt_fechar_agenda)
    public void fechar(){
        finish();
    }

}
