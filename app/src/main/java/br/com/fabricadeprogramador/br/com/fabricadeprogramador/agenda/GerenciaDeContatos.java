package br.com.fabricadeprogramador.br.com.fabricadeprogramador.agenda;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Everton on 17/10/2016.
 */

public class GerenciaDeContatos {
    private  static List<Pessoa> pessoaList = new ArrayList();

    public static void  adicionar(Pessoa pessoa){
        pessoaList.add(pessoa);
    }
    public static List<Pessoa> getNomes(){
        return  pessoaList;
    }

    public static List<Pessoa> getPessoaList() {
        return pessoaList;
    }

    public static void setPessoaList(List<Pessoa> pessoaList) {
        GerenciaDeContatos.pessoaList = pessoaList;
    }
}
