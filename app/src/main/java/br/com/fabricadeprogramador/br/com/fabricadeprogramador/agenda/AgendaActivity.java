package br.com.fabricadeprogramador.br.com.fabricadeprogramador.agenda;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import br.com.fabricadeprogramador.appfdp_online.R;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.R.attr.x;

/**
 * Created by Everton on 16/10/2016.
 */

public class AgendaActivity extends AppCompatActivity{
   @Bind(R.id.ed_nome)
    EditText edNome;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.agenda);
        ButterKnife.bind(this);
    }
    public  void _clear(){
        String x="";
    }
    @OnClick (R.id.btn_salvar)
    public void salvar(){

        String nome = edNome.getText().toString();
        _clear();
        Intent _goSalvar = new Intent(AgendaActivity.this,AgendaListaActivity.class);
        _goSalvar.putExtra("nome",nome);
        startActivity(_goSalvar);

    }
}

