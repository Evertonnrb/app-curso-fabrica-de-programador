package br.com.fabricadeprogramador.calculadora;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import br.com.fabricadeprogramador.appfdp_online.R;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.ContentValues.TAG;

/**
 * Created by Everton on 16/10/2016.
 */

public class Resultado extends Activity {

    @Bind(R.id.fechar)
    Button fechar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.resultado_calculo);
        //Ativando o ButterKnife
        ButterKnife.bind(this);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        Double calculo = bundle.getDouble("calculo");
        TextView txtR = (TextView)findViewById(R.id.txt_res);
        txtR.setText(calculo.toString());



    }
    //ButterKnif S2...
    @OnClick(R.id.fechar)
    public void fechar() {
        finish();
    }




    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG,"chamou  o onStart"+TAG);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(TAG,"chamou  o onReStart"+TAG);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG,"chamou  o Pause"+TAG);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG,"chamou  o Stop"+TAG);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG,"chamou  o Destroy"+TAG);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG,"chamou  o Resume"+TAG);
    }
}
