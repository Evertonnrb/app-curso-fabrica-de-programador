package br.com.fabricadeprogramador.calculadora;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import br.com.fabricadeprogramador.appfdp_online.R;

/**
 * Created by Everton on 16/10/2016.
 */

public class CalculadoraActivity extends Activity{

    public void alerta(View v){
        AlertDialog alertDialog;
        alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Resultado");
        alertDialog.setMessage("Error");
        alertDialog.show();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.calculadora);
        Button _somar = (Button)findViewById(R.id.bt_soma);
        _somar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText txt3 = (EditText)findViewById(R.id.resul);
                EditText txt1 = (EditText)findViewById(R.id.ed1);
                EditText txt2 = (EditText)findViewById(R.id.ed2);

                String _strN1 = txt1.getText().toString();
                String _strN2 = txt2.getText().toString();
                if((_strN1.isEmpty()||_strN2.isEmpty())&&(_strN2.isEmpty()||_strN1.isEmpty())){
                    Toast.makeText(CalculadoraActivity.this,"Por favor preencha os campos",Toast.LENGTH_SHORT).show();
                }else{
                    Double _n1 = Double.parseDouble(_strN1);
                    Double _n2 = Double.parseDouble(_strN2);
                    Double calculo = _n1+_n2;
                    //Toast.makeText(CalculadoraActivity.this,"Resultado = "+calculo,Toast.LENGTH_SHORT).show();
                    txt3.setText(calculo.toString());
                    //Indo para a view resultado
                   Intent intent = new Intent(CalculadoraActivity.this,Resultado.class);
                    Bundle bundle = new Bundle();
                    bundle.putDouble("calculo",calculo);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }

            }
        });

        Button _sub = (Button)findViewById(R.id.bt_sub);
        _sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText txt3 = (EditText)findViewById(R.id.resul);
                EditText txt1 = (EditText)findViewById(R.id.ed1);
                EditText txt2 = (EditText)findViewById(R.id.ed2);

                String _strN1 = txt1.getText().toString();
                String _strN2 = txt2.getText().toString();
                if((_strN1.isEmpty()||_strN2.isEmpty())&&(_strN2.isEmpty()||_strN1.isEmpty())){
                    Toast.makeText(CalculadoraActivity.this,"Por favor preencha os campos",Toast.LENGTH_SHORT).show();
                }else{
                    Double _n1 = Double.parseDouble(_strN1);
                    Double _n2 = Double.parseDouble(_strN2);
                    Double calculo = _n1-_n2;
                   // Toast.makeText(CalculadoraActivity.this,"Resultado = "+calculo,Toast.LENGTH_SHORT).show();
                    txt3.setText(calculo.toString());
                    Intent intent = new Intent(CalculadoraActivity.this,Resultado.class);
                    Bundle bundle = new Bundle();
                    bundle.putDouble("calculo",calculo);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }

            }
        });
        Button _div = (Button)findViewById(R.id.bt_div);
        _div.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText txt3 = (EditText)findViewById(R.id.resul);
                EditText txt1 = (EditText)findViewById(R.id.ed1);
                EditText txt2 = (EditText)findViewById(R.id.ed2);

                String _strN1 = txt1.getText().toString();
                String _strN2 = txt2.getText().toString();
                if((_strN1.isEmpty()||_strN2.isEmpty())&&(_strN2.isEmpty()||_strN1.isEmpty())){
                    Toast.makeText(CalculadoraActivity.this,"Por favor preencha os campos",Toast.LENGTH_SHORT).show();
                }else{
                    try{
                    Double _n1 = Double.parseDouble(_strN1);
                    Double _n2 = Double.parseDouble(_strN2);
                    Double calculo = _n1/_n2;
                        Intent intent = new Intent(CalculadoraActivity.this,Resultado.class);
                        Bundle bundle = new Bundle();
                        bundle.putDouble("calculo",calculo);
                        intent.putExtras(bundle);
                        startActivity(intent);
                        //Toast.makeText(CalculadoraActivity.this,"Resultado = "+calculo,Toast.LENGTH_SHORT).show();
                        txt3.setText(calculo.toString());

                    }

                    catch (Exception ex){
                        Toast.makeText(CalculadoraActivity.this," "+ex,Toast.LENGTH_SHORT).show();
                        txt3.setText(ex.toString());
                    }

                }

            }
        });
        Button _mult= (Button)findViewById(R.id.bt_mult);
        _mult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText txt3 = (EditText)findViewById(R.id.resul);
                EditText txt1 = (EditText)findViewById(R.id.ed1);
                EditText txt2 = (EditText)findViewById(R.id.ed2);

                String _strN1 = txt1.getText().toString();
                String _strN2 = txt2.getText().toString();
                if((_strN1.isEmpty()||_strN2.isEmpty())&&(_strN2.isEmpty()||_strN1.isEmpty())){
                    Toast.makeText(CalculadoraActivity.this,"Por favor preencha os campos",Toast.LENGTH_SHORT).show();
                }else{
                    Double _n1 = Double.parseDouble(_strN1);
                    Double _n2 = Double.parseDouble(_strN2);
                    Double calculo = _n1*_n2;
                    //Toast.makeText(CalculadoraActivity.this,"Resultado = "+calculo,Toast.LENGTH_SHORT).show();
                    txt3.setText(calculo.toString());
                    Intent intent = new Intent(CalculadoraActivity.this,Resultado.class);
                    Bundle bundle = new Bundle();
                    bundle.putDouble("calculo",calculo);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }

            }
        });


    }
}
