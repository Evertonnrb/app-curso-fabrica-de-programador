package br.com.fabricadeprogramador.appfdp_online;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import br.com.fabricadeprogramador.br.com.fabricadeprogramador.agenda.AgendaActivity;
import br.com.fabricadeprogramador.calculadora.CalculadoraActivity;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @Bind(R.id.btn_agenda) Button agenda;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        Button _calc = (Button)findViewById(R.id.btn_calc);
        _calc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent _go = new Intent(MainActivity.this,CalculadoraActivity.class);
                startActivity(_go);
            }
        });


    }
    @OnClick(R.id.btn_agenda)
    public void _irParaAgenda(){
        Intent _go = new Intent(MainActivity.this, AgendaActivity.class);
        startActivity(_go);
    }
}
